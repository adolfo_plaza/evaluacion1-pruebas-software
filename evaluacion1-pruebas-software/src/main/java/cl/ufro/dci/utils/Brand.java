package cl.ufro.dci.utils;

import java.util.Random;

/**
 * Brand enum where a random brand is selected for the car being generated
 */
public enum Brand {
    FORD,
    CHEVROLET,
    TOYOTA,
    HONDA,
    BMW,
    MERCEDES_BENZ,
    AUDI,
    VOLKSWAGEN;

    private static final Random random = new Random();

    public static Brand getRandomBrand() {
        return values()[random.nextInt(values().length)];
    }
}
