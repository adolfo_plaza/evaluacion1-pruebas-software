package cl.ufro.dci.utils;

import java.util.List;
import java.util.Random;

public class RandomGenerator {

    /**
     * Function that generates a random year from 2015 to 2023.
     * @return random year.
     */
    public int generateRandomYear() {
        Random random = new Random();
        return random.nextInt(9) + 2015;
    }

    /**
     * Function that generates a random price between 8 and 30 million.
     * @return random price.
     */
    public int generateRandomPrice() {
        Random random = new Random();
        return random.nextInt(29999992) + 8000000;
    }

    /**
     * Function that generates a random boolean.
     * @return random boolean.
     */
    public boolean generateRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }
    /**
     * Function that generates a random type.
     * @return random int.
     */
    public int generateRandomType(){
        Random random = new Random();
        return random.nextInt(3);
    }

    /**
     * Function that generates a random cabin number.
     * @return random int.
     */
    public int generateRandomcabins(){
        Random random = new Random();
        return random.nextInt(2)+1;
    }

    /**
     * Function that generates a random engine for the sedan vehicle
     * @return the random possible engine
     */
    public double generateRandomSedanEngine() {
        List<Double> possibleEngines = List.of(1.4, 1.6, 2.0);
        Random random = new Random();
        return possibleEngines.get(random.nextInt(possibleEngines.size()));
    }

    /**
     * Function that generates a random engine for the truck vehicle
     * @return the random possible engine
     */
    public double generateRandomTruckEngine() {
        List<Double> possibleEngines = List.of(2.4, 3.0, 4.0);
        Random random = new Random();
        double engine = possibleEngines.get(random.nextInt(possibleEngines.size()));
        return engine;
    }

    /**
     * Function that generates a random engine for the SUV vehicle
     * @return the random possible engine
     */
    public double generateRandomSUVEngine() {
        List<Double> possibleEngines = List.of(1.8, 2.2, 2.8);
        Random random = new Random();
        double engine = possibleEngines.get(random.nextInt(possibleEngines.size()));
        return engine;
    }
}
