package cl.ufro.dci.utils;

import java.util.Random;

/**
 * Color enum where a random color is selected for the car being generated
 */
public enum Color {
    CERULEAN_GREEN,
    YELLOW,
    FERRARI_RED,
    MATTE_GRAY,
    BLUE,
    EGG_WHITE,
    ORANGE,
    SAGE;

    private static final Random random = new Random();

    public static Color getRandomColor() {
        return values()[random.nextInt(values().length)];
    }
}
