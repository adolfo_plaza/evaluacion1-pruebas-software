package cl.ufro.dci;

import cl.ufro.dci.generator.CarGenerator;
import cl.ufro.dci.printer.CarPrinter;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        CarGenerator carGenerator = new CarGenerator();
        CarPrinter carPrinter = new CarPrinter();
        Scanner scanner = new Scanner(System.in);
        System.out.println("¿How many vehicles do you want to generate?");
        int carAmount = scanner.nextInt();
        carPrinter.printAllCars(carGenerator.generateRandomCars(carAmount));
    }
}