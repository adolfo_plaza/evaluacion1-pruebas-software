package cl.ufro.dci.generator;

import cl.ufro.dci.models.Car;
import cl.ufro.dci.models.PickUpTruck;
import cl.ufro.dci.models.SUV;
import cl.ufro.dci.utils.Brand;
import cl.ufro.dci.utils.Color;
import cl.ufro.dci.utils.RandomGenerator;

import java.util.UUID;

import java.util.ArrayList;

public class CarGenerator {

    /**
     * Funtion that generate a random car list depending on the amount of cars
     * the user wants to generate.
     * @param carAmount the amount of cars to generate.
     * @return List of random generated cars.
     */
    public ArrayList<Car> generateRandomCars(int carAmount){
        ArrayList<Car> carsList = new ArrayList<>();
        RandomGenerator randomGenerator = new RandomGenerator();
        for(int i = 0; i < carAmount; i++){
            Car car = typeGenerator(randomGenerator);
            car.setId(UUID.randomUUID());
            car.setBrand(Brand.getRandomBrand());
            car.setYear(randomGenerator.generateRandomYear());
            car.setColor(Color.getRandomColor());
            car.setPrice(randomGenerator.generateRandomPrice());
            car.setTurbo(randomGenerator.generateRandomBoolean());
            carsList.add(car);
        }
        return carsList;
    }
    /**
     * Generates a random type of car, and fills in type-specific car data
     * @param generator the random generator.
     * @return A variety of Car.
     */
    public Car typeGenerator(RandomGenerator generator){
        switch(generator.generateRandomType()){
            case 0:
                Car car = new Car();
                car.setEngine(generator.generateRandomSedanEngine());
                return car;
            case 1:
                PickUpTruck truck = new PickUpTruck(generator.generateRandomcabins());
                truck.setEngine(generator.generateRandomTruckEngine());
                return truck;
            case 2:
                SUV suv = new SUV(generator.generateRandomBoolean());
                suv.setEngine(generator.generateRandomSUVEngine());
                return suv;
        }
        return null;
    }
}
