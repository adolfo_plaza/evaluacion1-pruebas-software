package cl.ufro.dci.models;


public class PickUpTruck extends Car{

    private int cabins;

    public PickUpTruck( int cabins){
        this.cabins = cabins;
    }

    public int getCabins(){
        return this.cabins;
    }

    public void setCabins(int cabins){
        this.cabins = cabins;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "id= " + super.getId() +
                ", price= " + super.getPrice() +
                ", brand= " + super.getBrand() +
                ", year= '" + super.getYear() + '\'' +
                ", color= '" + super.getColor() + '\'' +
                ", engine= " + super.getEngine() +
                "cc, turbo= " + super.isTurbo() +
                ", cabin= "+ cabins+"}";
    }
}
