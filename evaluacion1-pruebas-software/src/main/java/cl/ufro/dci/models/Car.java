package cl.ufro.dci.models;

import cl.ufro.dci.utils.Brand;
import cl.ufro.dci.utils.Color;

import java.util.UUID;

/**
 * Car Model where the user can specify the id, price, brand, year, color, type, engine and turbo
 */
public class Car {

    private UUID id;
    private long price;
    private Brand brand;
    private int year;
    private Color color;
    private String type;
    private double engine;
    private boolean turbo;

    public Car(UUID id, long price, Brand brand, int year, Color color,String type, double engine, boolean turbo) {
        this.id = id;
        this.price = price;
        this.brand = brand;
        this.year = year;
        this.color = color;
        this.type = type;
        this.engine = engine;
        this.turbo = turbo;
    }

    public Car(){
        this.id = null;
        this.price = 0;
        this.brand = null;
        this.year = 0;
        this.color = null;
        this.type = null;
        this.engine = 0;
        this.turbo = false;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getEngine() {
        return engine;
    }

    public void setEngine(double engine) {
        this.engine = engine;
    }

    public boolean isTurbo() {
        return turbo;
    }

    public void setTurbo(boolean turbo) {
        this.turbo = turbo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", price=" + price +
                ", brand=" + brand +
                ", year=" + year +
                ", color=" + color +
                ", type='" + type + '\'' +
                ", engine=" + engine +
                "cc, turbo=" + turbo +
                '}';
    }
}
