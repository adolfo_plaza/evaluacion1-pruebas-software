package cl.ufro.dci.models;

public class SUV extends Car {
    private boolean sunRoof;

    public SUV( boolean sunRoof){
        this.sunRoof = sunRoof;
    }

    public boolean getSunRoof(){
        return this.sunRoof;
    }

    public void setSunRoof(boolean sunRoof){
        this.sunRoof = sunRoof;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "id= " + super.getId() +
                ", price= " + super.getPrice() +
                ", brand= " + super.getBrand() +
                ", year= '" + super.getYear() + '\'' +
                ", color= '" + super.getColor() + '\'' +
                ", engine= " + super.getEngine() +
                "cc, turbo= " + super.isTurbo() +
                ",sunroof= "+sunRoof+"}";
    }
}
