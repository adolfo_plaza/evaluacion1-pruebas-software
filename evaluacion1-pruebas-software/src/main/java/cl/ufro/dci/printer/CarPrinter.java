package cl.ufro.dci.printer;

import cl.ufro.dci.models.Car;

import java.util.ArrayList;

public class CarPrinter {

    /**
     * Funtion that prints on the console all the cars given in the ArrayList
     * @param carList List of cars received
     */
    public void printAllCars(ArrayList<Car> carList) {
        for (Car car : carList) {
            System.out.println(car.toString());
        }
    }
}
